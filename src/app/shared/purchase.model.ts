export interface Purchase {
    quantidade: number;
    preco: number;
    data: string;
    produto: number;
}