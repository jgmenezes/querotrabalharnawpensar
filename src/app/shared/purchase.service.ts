import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { GenericService } from './generic.service';

import { Purchase } from './purchase.model';

@Injectable()
export class PurchaseService extends GenericService<Purchase> {
    private _url: string = 'https://private-anon-ea22b2b522-querotrabalharnawpensar.apiary-mock.com/api/compras'; /* URL da API de compras */

    constructor(private _HTTP: HttpClient) {
        super(_HTTP);
    }

    addPurchase(product: Purchase): Observable<Purchase> {
        return this.add(this._url, product);
    }

    getPurchaseById(id: number): Observable<Purchase> {
        return this.getById(this._url, id);
    }
    
    getAllPurchases(): Observable<Purchase[]> {
        return this.getAll(this._url)
    }
}