import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { GenericService } from './generic.service';

import { Product } from './product.model';

@Injectable()
export class ProductService extends GenericService<Product> {
    private _url: string = 'https://private-anon-ea22b2b522-querotrabalharnawpensar.apiary-mock.com/api/produtos'; /* URL da API de produtos*/

    constructor(private _HTTP: HttpClient) {
        super(_HTTP);
    }

    addProduct(product: Product): Observable<Product> {
        return this.add(this._url, product);
    }

    getProductById(id: number): Observable<Product> {
        return this.getById(this._url, id);
    }

    getAllProducts(): Observable<Product[]> {
        return this.getAll(this._url);
    }
    
}