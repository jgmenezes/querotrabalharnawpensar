/* Criei um service genérico pois os métodos se repetem nos serviços
   de produto e compra, desta forma, posso reutilizá-los */
 
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const _httpOptions = { 
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export class GenericService<T> {
    constructor(private _http: HttpClient) { }
    
    /*
        Utilizei observables pois responde continuamente às mudanças no item e, além disso,
        caso precisasse poderia tratar os dados usando map, filter, reduce, retry, etc.
    */

    /* Este método é um adicionar genérico.*/
    add(url: string, item: T): Observable<T> {
        return this._http.post<T>(url, item, _httpOptions)
    }
    
    /* Este método é um obter todos genérico */
    getAll(url: string): Observable<T[]> {
        return this._http.get<T[]>(url, _httpOptions)
    }

    /* Este método é um obter por id genérico */
    getById(url: string, id: number): Observable<T> {
        return this._http.get<T>(`${url}/${id}`, _httpOptions)
    }
}