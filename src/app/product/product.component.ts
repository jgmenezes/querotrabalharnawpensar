import { Product } from './../shared/product.model';
import { Component, OnInit } from '@angular/core';

import { ProductService } from './../shared/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  private _message: string = ''; /* mensagem que aparece na tela ao ser cadastrado */
  private _name: string = '';

  constructor(private productService: ProductService) { }

  ngOnInit() {
    
  }

  /* Método para adicionar produto */
  addProduct(product: Product) {
    if(this._name != '') {
      this.productService.addProduct(product)
        .subscribe( 
          () => { 
            this._message = 'Produto cadastrado com sucesso.',
            //voltando os campos da tela para o estado inicial, limpando a tela
            this._name = ''
          }, 
          error => this._message = 'Error: ' + error)
    }
    else 
      this._message = 'Campo inválido.'
  }
}
