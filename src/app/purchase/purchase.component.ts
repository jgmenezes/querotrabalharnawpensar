import { Component, OnInit } from '@angular/core';

import { Purchase } from './../shared/purchase.model';
import { Product } from './../shared/product.model';

import { PurchaseService } from './../shared/purchase.service';
import { ProductService } from './../shared/product.service';

import 'rxjs/operators/map';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {
  private _message: string = ''; /* mensagem que aparece na tela ao ser cadastrado */
  private _productsList: Array<Product>;
  private productId: number = -39020631; //começa com o valor do id do primeiro item da lista
  private amount: number;
  private price: number;

  constructor(private _productService: ProductService, private _PurchaseService: PurchaseService) { }

  ngOnInit() {
    /* inserindo todos os produtos na variável _productsList para ser iterada na tag <select> ao componente ser carregado */
    this._productService.getAllProducts()
      .subscribe( products => this._productsList = products )
  }

  /* Método para adicionar compra. */
  addBuy(buy: Purchase) { 
    if(this.productId != undefined && this.amount != undefined && this.price != undefined) {
      this._PurchaseService.addPurchase(buy)
        .subscribe(
          () => {
            this._message = 'Compra efetuada com sucesso.'
            //voltando os campos da tela para o estado inicial, limpando a tela
            this.productId = -39020631; //id do primeiro item da lista;
            this.amount = undefined;
            this.price = undefined;
          }, 
          error => this._message = 'Error: ' + error
        )
    }
    else
      this._message = 'Campo(s) inválido(s).'
  }
}
