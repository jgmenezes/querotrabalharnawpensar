import { Component, OnInit } from '@angular/core';

import { PurchaseService } from './../shared/purchase.service';
import { Purchase } from './../shared/purchase.model';

@Component({
  selector: 'app-purchase-list',
  templateUrl: './purchase-list.component.html',
  styleUrls: ['./purchase-list.component.css']
})
export class PurchaseListComponent implements OnInit {
  private _purchasesList: Array<Purchase>;

  constructor(private _purchaseService: PurchaseService) { }

  ngOnInit() {
    /* 
       Inserindo todas as compras na variável _purchasesList ao componente ser carregado.
       Se ocorrerem alterações, ou seja, se alguma compra for adicionada, editada ou removida 
       a lista também sofrerá essa alteração. O subscribe fica "escutando" e assim que a resposta vier
       atualiza o array. 
     */
    this._purchaseService.getAllPurchases()
      .subscribe(purchases => this._purchasesList = purchases)
  }

}
