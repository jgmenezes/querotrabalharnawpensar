// arquivo com as rotas do projeto

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ProductComponent } from './product/product.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { PurchaseListComponent } from './purchase-list/purchase-list.component';

const appRoutes: Routes = [
    { path: '', component: MainComponent },
    { path: 'cadastrar-produto', component: ProductComponent },
    { path: 'comprar-produto', component: PurchaseComponent },
    { path: 'listar-produtos', component: PurchaseListComponent }
]

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }